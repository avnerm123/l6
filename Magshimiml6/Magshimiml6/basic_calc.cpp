#include <iostream>

#define NOT_ALLOWED 8200
#define MSG "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year\n"

int add(int a, int b) {
	if (a == NOT_ALLOWED || b == NOT_ALLOWED || a + b == NOT_ALLOWED)
	{
		std::cout << MSG;
		return NOT_ALLOWED;
	}
	return a + b;
}

int  multiply(int a, int b) {
	
	int sum = 0;
	for(int i = 0; i < b; i++) {
		sum = add(sum, a);
		if (sum == NOT_ALLOWED)
		{
			return NOT_ALLOWED;
		}
	};
	if (a == NOT_ALLOWED || b == NOT_ALLOWED)
	{
		return NOT_ALLOWED;
	}
	return sum;
}

int  pow(int a, int b) {
	
	int exponent = 1;
		for (int i = 0; i < b; i++) {
			exponent = multiply(exponent, a);
			if (exponent == NOT_ALLOWED)
			{
				return NOT_ALLOWED;
			}
		};
		if (a == NOT_ALLOWED || b == NOT_ALLOWED)
		{
			return NOT_ALLOWED;
		}
	return exponent;
}

int main(void) {
	std::cout << "adding 8000, 200:" << add(8000, 200) << std::endl;
	std::cout << "adding 20, 20:" << add(20, 20) << std::endl;
	std::cout << "adding 8200, -200:" << add(NOT_ALLOWED, -200) << std::endl;

	std::cout << "mul 8200, 1:" << multiply(NOT_ALLOWED, 1) << std::endl;
	std::cout << "mul 205, 41:" <<multiply(205, 41) << std::endl;
	std::cout << "mul 205, 40:" << multiply(205, 40) << std::endl;
	std::cout << "mul 20, 20:" << multiply(20, 20) << std::endl;

	std::cout << "pow 8200, 1:" << pow(NOT_ALLOWED, 2) << std::endl;
	std::cout << "pow 5, 5:" << pow(5, 5) << std::endl;
}